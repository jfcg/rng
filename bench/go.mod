module github.com/jfcg/rng/bench

go 1.17

require (
	github.com/jfcg/rng v1.0.1
	golang.org/x/exp v0.0.0-20220613132600-b0d781184e0d
)

require github.com/jfcg/sixb v1.3.5 // indirect

replace github.com/jfcg/rng => ../
